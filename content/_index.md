---
date: 2022-10-25
---
Zouz
Isabelle de Sainte-Marie
Marseille
## Compétences
### Admin système & réseaux
Ubuntu, Debian, ansible, apache, tomcat, nginx, mysql, postgresql, postfix, iptables, DNS, Zabbix, Grafana, Time Navigator, VMWare, HAProxy, ProxySQL, PgBouncer
### Codes
bash, php, python, perl, javascript, ruby
### Web et multimédia
html, css, xml, xslt, Gimp, Inkscape, Avidemux, Audacity, 
## Formations
- 2021 : Ansible
- 1989 - 1991 : DUT Chimie à l'IUT de Montpellier
- 1987 : Baccalauréat C
## Emplois
2012- maintenant : Admin sys à l'Université d'Aix-Marseille
2005-2012 : Webmaster, admin système web à l'Université de Provence
1999-2005 : Graphiste, webmaster à l'Université de Provence
